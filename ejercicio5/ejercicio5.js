alert("Gracias por usar mi calculadora :-) (RPARTIDA81)");
var pri_op; //variable global que almacena el primer operando.
var seg_op; //variable global que almacena el segundo operando.
var operacion; //variable global que almacena la operación.
var t_op; //testigo durante una operación.
var t_igual; //testigo ya hemos pulsado igual.

function raiz(){ //función que realiza la raiz cuadrada.
	if (t_op==1){ //compruebo que no esta en medio de una operación.
	document.getElementById("pantalla").value = "Error estas en medio de una operación";
	}else{
	pri_op=document.getElementById("pantalla").value; //almacenamos el valor en pri_op
	if (pri_op<0){ //comprobamos que no sea negativo.
		document.getElementById("pantalla").value = "Error raiz negativa...";
	} else{
		var resultado = Math.sqrt(pri_op); // Función de la clase Math para hacer la raiz cuadrada.
		document.getElementById("pantalla").value = resultado; //muestro en pantalla el resultado.
		}
	}
}

function numero(tmp){ //función que captura cada número pulsado
	var temp = parseFloat(tmp); //almaceno valor pasado como parametro en variable temporal.
	var pantalla = document.getElementById("pantalla").value; //almaceno contenido de la pantalla
	document.getElementById("pantalla").value = pantalla + temp; //al valor anterior le añado el nuevo número pulsado.
}

function coma(){ //función que añade una coma (.) 
	var temp = document.getElementById("pantalla").value; //almaceno el contenido de la pantalla.
	if (temp.indexOf('.') ==-1){ //si no hay ninguna coma(punto) en la pantalla puede continuar
		var pantalla = document.getElementById("pantalla").value; //almaceno el contenido de la pantalla.
		document.getElementById("pantalla").value = pantalla + "."; //le añado la coma (punto) al valor almacenado anteriormente.
	}
}

function borrar(){ //función para limpiar la pantalla.
	document.getElementById("pantalla").value = ""; //el nuevo valor de la pantalla es una cedena vacia.
}

function suma(){ //función de suma.
	if (t_op!==1){ //si el testigo de que estamos durante una operación no vale 1 continua.
	pri_op = document.getElementById("pantalla").value; //almaceno el valor de primer operando.
	operacion = "suma"; // almaceno en la variable operacion el nombre de la operación a realizar.
	t_op=1; //testigo de operacion en curso en 1.
	t_igual=0; //testigo para poder darle a igual en 0.
	borrar(); //borramos la pantalla.
	} else {
		document.getElementById("pantalla").value = "Error estas a mitad de una operación"; //Error al dar a alguna operación durante operación.
	}
}

function resta() { //función resta.
	if (t_op!==1){
	pri_op = document.getElementById("pantalla").value;
	operacion = "resta";
	t_op=1;
	t_igual=0;
	borrar();
	} else {
		document.getElementById("pantalla").value = "Error estas a mitad de una operación";
	}
}

function multiplicacion() { //función multiplicación.
	if (t_op!==1){
	pri_op = document.getElementById("pantalla").value;
	operacion = "multiplicacion";
	t_op=1;
	t_igual=0;
	borrar();
	} else {
		document.getElementById("pantalla").value = "Error estas a mitad de una operación";
	}
}

function division() { //función división.
	if (t_op!==1){
	pri_op = document.getElementById("pantalla").value;
	operacion = "division";
	t_op=1;
	t_igual=0;
	borrar();
	} else {
		document.getElementById("pantalla").value = "Error estas a mitad de una operación";
	}
}

function potencia() { //función potencia.
	if (t_op!==1){
	pri_op = document.getElementById("pantalla").value;
	operacion = "potencia";
	t_op=1;
	t_igual=0;
	borrar();
	} else {
		document.getElementById("pantalla").value = "Error estas a mitad de una operación";
	}
}

function cambioSigno(){ //función para cambiar el signo.
	var pantalla = document.getElementById("pantalla").value;
	document.getElementById("pantalla").value = pantalla *-1;
	
}

function resultado(){ //función que nos da el resultado de las operaciones.
	if (document.getElementById("pantalla").value==""){ //comprueba que se ha introducido un valor para el segundo operando.
		document.getElementById("pantalla").value = "Error debe poner el segundo operando";
	} else if (t_igual==1){ //No hace nada si se da más veces al botón igual.
	} else{
	switch (operacion){ //selector de operación a realizar según el valor de la var operacion.
		case "suma":
			seg_op = document.getElementById("pantalla").value; // se almacena el valor del segundo operando.
			document.getElementById("pantalla").value = parseFloat(pri_op)+parseFloat(seg_op); //Uso parse para que no concatene.
			t_op=0; // Reiniciamos el testigo de operación
			t_igual=1; // Activamos el testigo del botón igual.
			break;
		case "resta":
			seg_op = document.getElementById("pantalla").value;
			document.getElementById("pantalla").value = parseFloat(pri_op)-parseFloat(seg_op);
			t_op=0;
			t_igual=1;
			break;
		case "multiplicacion":
			seg_op = document.getElementById("pantalla").value;
			document.getElementById("pantalla").value = parseFloat(pri_op)*parseFloat(seg_op);
			t_op=0;
			t_igual=1;
			break;
		case "potencia":
			seg_op = document.getElementById("pantalla").value;
			document.getElementById("pantalla").value = Math.pow(pri_op,seg_op); //Usamos la función de la clase Math para la operación.
			t_op=0;
			t_igual=1;
			break;
		case "division":
			seg_op = document.getElementById("pantalla").value;
			if (seg_op==0){ //Evitamos error división entre 0.
			document.getElementById("pantalla").value = "Error división entre 0"; //Aviso error división entre 0.
			}else{
			document.getElementById("pantalla").value = parseFloat(pri_op)/parseFloat(seg_op);
			t_op=0;
			t_igual=1;
			break;
			}
		}
	}
}

//Código Creado por Rafael Partida Ibáñez (Evaluable 1 DWEC).