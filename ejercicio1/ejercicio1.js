//creo función mostrarJugadores
function mostrarJugadores(){
//vectores con los nombres de jugadores y values.
var id_jug = ["jugador1", "jugador2", "jugador3", "jugador4", "jugador5"];
var jug_e1 = ["Valdés", "Sisako", "Nosako", "Avecesako", "Nuncasako"];
var jug_e2 = ["Pelé", "Maradona", "Valderrama", "Chuster", "Butragueño"];
var jug_e3 = ["Cruif", "Luis Enrique", "Romario", "Zubizarreta", "Guardiola"];
var jug_e4 = ["Roberto Carlos", "Cristiano", "Pepe", "Iker", "James"];

//obtener el elemento de "equipos" que esta seleccionado
var equipos = document.getElementById("equipos"); //obtengo la lista de opciones
var i_equipo = equipos.selectedIndex; //consigo la posición del elemento seleccionado
var equipof = equipos.options[i_equipo]; // tengo el elemento
var ver_equipo = equipof.value; // consigo el valor del atributo value.

//almaceno el elemento padre de todas las option
var padre = document.getElementById("jugadores"); 

switch (ver_equipo){
	case "equipo1":
		padre.options.length=1; //dejo solo el primer elemento del select
		for(i=0;i<jug_e1.length;i++){	//bucle para rellenar los 5 elementos
		var tmp = document.createElement("option"); //Creamos el elemento option
		tmp.value=id_jug[i]; //Al elemento option le añado el atributo value
		tmp.text=jug_e1[i]; //Al elemento option le añado el texto o valor.
		padre.appendChild(tmp); //coloco el elemento option como hijo del elemento select.
		}
		break;
	case "equipo2":
		padre.options.length=1;
		for(i=0;i<jug_e2.length;i++){
		var tmp = document.createElement("option");
		tmp.value=id_jug[i];
		tmp.text=jug_e2[i];
		padre.appendChild(tmp);
		}
		break;
	case "equipo3":
		padre.options.length=1;
		for(i=0;i<jug_e3.length;i++){
		var tmp = document.createElement("option");
		tmp.value=id_jug[i];
		tmp.text=jug_e3[i];
		padre.appendChild(tmp);
		}
		break;
	case "equipo4":
		padre.options.length=1;
		for(i=0;i<jug_e4.length;i++){
		var tmp = document.createElement("option");
		tmp.value=id_jug[i];
		tmp.text=jug_e4[i];
		padre.appendChild(tmp);
		}
		break;
	default: //cuando no se eligen ninguno de los 4 equipos se borran los elementos option de los jugadores.
		padre.options.length=1;
		break;
}
}

//Código Creado por Rafael Partida Ibáñez (Evaluable 1 DWEC).