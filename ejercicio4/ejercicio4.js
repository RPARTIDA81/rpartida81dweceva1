var contador = [0,0,0,0,0,0]; //vector contador de los dados que van saliendo
var caras = ["uno","dos","tres","cuatro","cinco","seis"]; //vector de texto que mostraré en el resultado.
var mayor=0; //variable que iré rellenando en el for con el dado que más veces ha salido.
var pos; //almaceno la posición del dado que más ha salido.

var idA = setInterval("mostrarDado();",1000); //fijo un intervalo de 1s para que ejecute la función.
var idB = setTimeout("contar();",20000); // al pasar 20s ejecuta la función.	

function contar(){ //función que cuenta que dado ha salido más veces.
clearInterval(idA); //cierra el setInterval
for (i=0;i<contador.length;i++){ //recorre el vector contador para ver el elemento mayor.
	if (contador[i]>mayor){
		mayor = contador[i];
		pos = i;
		}
	}
	alert ("Ha salido más veces el dado "+caras[pos]); //Mensaje final que muestra el resultado.
}

function mostrarDado(){ //función que genera un número aleatorio 1-6 y según el valor muestra la imagen del dado correspondiente.
var dado = Math.floor((Math.random()*6) + 1);
switch (dado){
	case 1:
		document.getElementById("imagen").src = "1.jpg"; //modifica el atributo src de la etiqueta img.
		contador[dado-1]++; // aumenta en 1 el contador 
		break;
	case 2:
		document.getElementById("imagen").src = "2.jpg";
		contador[dado-1]++;
		break;
	case 3:
		document.getElementById("imagen").src = "3.jpg";
		contador[dado-1]++;
		break;
	case 4:
		document.getElementById("imagen").src = "4.jpg";
		contador[dado-1]++;
		break;
	case 5:
		document.getElementById("imagen").src = "5.jpg";
		contador[dado-1]++;
		break;
	case 6:
		document.getElementById("imagen").src = "6.jpg";
		contador[dado-1]++;
		break;	
	}
}

//Código Creado por Rafael Partida Ibáñez (Evaluable 1 DWEC).